#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

while :
   do
      echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
      read -r NUMBER

         if [ "$NUMBER" -lt 1 ];
            then
               echo "You must enter a number that's >= 1"
               continue
         fi
               
         if [ "$NUMBER" -gt "$THE_MAX_VALUE" ];
            then
               echo "You must enter a number that's <= $THE_MAX_VALUE"
               continue
         fi       

         if [ "$NUMBER" -gt "$THE_NUMBER_IM_THINKING_OF" ];
            then
               echo "No cat... the number I'm thinking of is smaller than $NUMBER" 
               continue
         fi 

         if [ "$NUMBER" -lt "$THE_NUMBER_IM_THINKING_OF" ];
            then
               echo "No cat... the number I'm thinking of is larger than $NUMBER"
               continue
         fi

         if [ "$NUMBER" -eq "$THE_NUMBER_IM_THINKING_OF" ];
            then
               echo "You got me"
               echo "|\---/|"
               echo "| o_o |"
               echo " \_^_/"
               break
         fi
   done


